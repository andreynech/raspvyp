# RaspVyp - resource-aware distributed task scheduler

RaspVyp could start arbitrary application on one of the computers available
over the network. Decision about where exactly to run application is made
automatically based on the performance metrics (CPU, memory, disk and
network load) of each particular host.

To participate in the distributed scheduling, each target host should run
_raspvyp_ daemon. It offers gRPC interface to collect host load metrics and
manage applications (start, stop, suspend, resume). _raspvyp_ does not aware
about any other _raspvyp_ applications running across the network and
manages only application on it's own host. However _raspvyp_ daemons are
discoverable with Zeroconf protocol.

Decission about where to run application is made by _cli_ - command line
application. _cli_ uses
[Zeroconf](https://en.wikipedia.org/wiki/Zero-configuration_networking)
(aka. Bonjour or Avahi) protocol to discover available target hosts (hosts,
where _raspvyp_ daemon is running). After collecting information about
available hosts, _cli_ queries load metrics. Host with less load is selected
to execute application. After that _cli_ instructs (over gRPC) corresponding
_raspvyp_ daemon to run application.

# Notes on compilation

This project supports modules (Go v.1.11 required). So it is possible to
build it outside of _$GOPATH_.

Since _SIGSTOP_ and _SIGCONT_ are not supported on Windows, there is
Windows-specific implementation of suspend and resume methods using Cgo. So
to compile it on Windows, make sure that gcc is installed (we recommend
[TDM-GCC](http://tdm-gcc.tdragon.net/)). Proper implementation is selected
for compilation automatically according to +build tags.

# Usage

Assuming we have host1 and host2 where _raspvyp_ daemon is running and
_host3_ where _cli_ client will be executed. _raspvyp_ daemon could be
started without parameters and by default will listen on all available
network interfaces:
```
host1 $ ./raspvyp
host2 $ ./raspvyp
```
Now we can run _cli_ on host3. By default, _cli_ will report about target
hosts discovered with Zeroconf.

Here is the list of options supported by _cli_:
```
$ ./cli --help
Usage of D:\H\raspvyp\cli\cli.exe:
  -c string
        The command to execute
  -domain string
        Set the search domain. For local networks, default is fine. (default "local.")
  -g string
        Control group
  -h string
        Target host name
  -l    List all running tasks
  -p int
        Process id (default -1)
  -r    Resume task. -i and -h parameters are required in this case
  -s    Start task. -c and -a arguments are required in this case
  -t    Stop the task. -i and -h parameters are required in this case
  -wait int
        Duration in [s] to run discovery. (default 3)
  -x    Remove all running tasks. If no -h provided, all hosts will be contacted
  -z    Suspend task. -i and -h parameters are required in this case
```

The following command could be used to start command on one of the participating hosts:
```
./cli -s -g subsystems:path_to_cgroup -c "sleep 100"
```
It will generate the following output:
```
2018/09/26 11:04:55 Discovering runners _raspvyp._tcp.local. (3 sec)...
2018/09/26 11:04:55 Discovered  local.  _raspvyp._tcp.  RaspVyp tutnix   56876
2018/09/26 11:04:55 Connected to tutnix.local.:56876
2018/09/26 11:04:58 1 hosts with runners found
2018/09/26 11:04:58 Task started with ID: 6856 on host tutnix
```
This output illustrates that one participated host _tutnix_ was discovered.
Command was successfully started on it and PID is 6856. PID and host name
could be used later to stop/suspend/resume the command.

To see what commands are currently running on all hosts, the following
command could be executed:
```
$ ./cli -l
2018/09/26 11:08:55 Discovering runners _raspvyp._tcp.local. (3 sec)...
2018/09/26 11:08:55 Discovered  local.  _raspvyp._tcp.  RaspVyp tutnix   56876
2018/09/26 11:08:55 Connected to tutnix.local.:56876
2018/09/26 11:08:58 1 hosts with runners found
2018/09/26 11:08:58 On tutnix
2018/09/26 11:08:58 pid: 6856 cgroup: asdf command: sleep 100
```
It shows that _sleep 100_ command is running on host _tutnix_


