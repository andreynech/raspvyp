// +build windows

package main

/*
#include <windows.h>

typedef LONG (NTAPI *NtSuspendProcess)(IN HANDLE ProcessHandle);
typedef LONG (NTAPI *NtResumeProcess)(IN HANDLE ProcessHandle);

void suspend(DWORD processId)
{
    HANDLE processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processId);

    NtSuspendProcess pfnNtSuspendProcess = (NtSuspendProcess)GetProcAddress(
        GetModuleHandle("ntdll"), "NtSuspendProcess");

    pfnNtSuspendProcess(processHandle);
    CloseHandle(processHandle);
}

void resume(DWORD processId)
{
    HANDLE processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processId);

    NtResumeProcess pfnNtResumeProcess = (NtResumeProcess)GetProcAddress(
        GetModuleHandle("ntdll"), "NtResumeProcess");

    pfnNtResumeProcess(processHandle);
    CloseHandle(processHandle);
}
*/
import "C"

import (
	rv "gitlab.com/andreynech/raspvyp/proto"
)

func suspendImpl(req *rv.Task) error {
	taskMutex.Lock()
	defer taskMutex.Unlock()

	C.suspend(C.ulong(req.GetId()))
	return nil
}

func resumeImpl(req *rv.Task) error {
	taskMutex.Lock()
	defer taskMutex.Unlock()

	C.resume(C.ulong(req.GetId()))
	return nil
}
