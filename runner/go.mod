module gitlab.com/andreynech/raspvyp/runner

require (
	cloud.google.com/go v0.28.0 // indirect
	github.com/brutella/dnssd v0.0.0-20180519095852-a1eecd10aafc
	github.com/miekg/dns v1.0.8 // indirect
	gitlab.com/andreynech/raspvyp/proto v0.0.0-20180924132106-dddab55cb3b7
	golang.org/x/crypto v0.0.0-20180910181607-0e37d006457b // indirect
	golang.org/x/net v0.0.0-20180921000356-2f5d2388922f
	golang.org/x/sys v0.0.0-20180921163948-d47a0f339242 // indirect
	golang.org/x/tools v0.0.0-20180917221912-90fa682c2a6e // indirect
	google.golang.org/appengine v1.2.0 // indirect
	google.golang.org/genproto v0.0.0-20180918203901-c3f76f3b92d1 // indirect
	google.golang.org/grpc v1.15.0
	honnef.co/go/tools v0.0.0-20180920025451-e3ad64cb4ed3 // indirect
)
