package main

import (
	"context"
	"github.com/brutella/dnssd"
	"log"
	"time"
)

func registerServer(port int, c <-chan bool) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	if resp, err := dnssd.NewResponder(); err != nil {
		log.Fatalln(err)
	} else {
		srv := dnssd.NewService("RaspVyp", "_raspvyp._tcp.", "local.", "", nil, port)

		go func() {
			time.Sleep(100 * time.Millisecond)
			handle, err := resp.Add(srv)
			if err != nil {
				log.Fatalln(err)
			} else {
				log.Printf("Got a reply for service %s:\n", handle.Service().ServiceInstanceName())
				log.Printf("Name now registered and active\n")
			}
		}()

		err = resp.Respond(ctx)

		if err != nil {
			log.Fatalln(err)
		}
		// Wait for shutdown command
		select {
		case <-c:
			log.Println("Uninitializing Zeroconf")
			cancel()
		}
	}
}
