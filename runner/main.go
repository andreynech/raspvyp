package main

import (
	"flag"
	rv "gitlab.com/andreynech/raspvyp/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
)

// server is used to implement raspvyp.Runner
type server struct{}

var (
	endpoint *string
)

func (s *server) Stat(ctx context.Context, req *rv.Empty) (*rv.HostStat, error) {
	response, error := statImpl()
	if error != nil {
		log.Printf("Error processing Stat request: %v", error)
	}
	return response, error
}

func (s *server) List(ctx context.Context, req *rv.Empty) (*rv.TaskList, error) {
	response, error := listImpl()
	if error != nil {
		log.Printf("Error processing List request: %v", error)
	}
	return &response, error
}

func (s *server) Start(ctx context.Context, req *rv.Task) (*rv.Task, error) {
	response, error := startImpl(req)
	if error != nil {
		log.Printf("Error processing Start request: %v", error)
	}
	return response, error
}

func (s *server) Stop(ctx context.Context, req *rv.Task) (*rv.Empty, error) {
	error := stopImpl(req)
	if error != nil {
		log.Printf("Error processing Stop request: %v", error)
	}
	return &rv.Empty{}, error
}

func (s *server) Suspend(ctx context.Context, req *rv.Task) (*rv.Empty, error) {
	error := suspendImpl(req)
	if error != nil {
		log.Printf("Error processing Suspend request: %v", error)
	}
	return &rv.Empty{}, error
}

func (s *server) Resume(ctx context.Context, req *rv.Task) (*rv.Empty, error) {
	error := resumeImpl(req)
	if error != nil {
		log.Printf("Error processing Resume request: %v", error)
	}
	return &rv.Empty{}, error
}

func (s *server) RemoveAll(ctx context.Context, req *rv.Empty) (*rv.Empty, error) {
	error := removeAllImpl()
	if error != nil {
		log.Printf("Error processing RemoveAll request: %v", error)
	}
	return &rv.Empty{}, error
}

func (s *server) OutputStream(req *rv.Task, stream rv.Runner_OutputStreamServer) error {
	err := outputStreamImpl(req, stream)
	if err != nil {
		log.Printf("Error processing OutputStream request: %v", err)
	}
	return err
}

func main() {
	// Parse command line parameters
	endpoint = flag.String("p", ":", "Endpoint to listen in format hostname:port")
	flag.Parse()

	lis, err := net.Listen("tcp", *endpoint)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	} else {
		log.Printf("Listening on address %s\n", lis.Addr())
	}

	s := grpc.NewServer()
	rv.RegisterRunnerServer(s, &server{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	// Register for discovery
	c := make(chan bool, 1)
	go registerServer(lis.Addr().(*net.TCPAddr).Port, c)

	// Prepare for clean shutdown
	sig := make(chan os.Signal, 1)
	signal.Notify(sig,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	go func() {
		log.Println("Listening for signals")
		<-sig
		log.Println("Got signal")
		c <- true
		log.Println("Stopping server")
		s.GracefulStop()
	}()

	// Serve requests
	log.Println("Server started")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
