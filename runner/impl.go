package main

import (
	"errors"
	rv "gitlab.com/andreynech/raspvyp/proto"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"
	"sync"
	"syscall"
)

type processInfo struct {
	task *rv.Task
	out  []chan string
	err  []chan string
}

// Global task list container
var (
	globalTaskList = make(map[uint32]processInfo)
)

// Mutex to synchronize global task list access
var taskMutex = sync.Mutex{}

func statImpl() (*rv.HostStat, error) {
	host, err := os.Hostname()
	if err != nil {
		log.Println(err)
		host = ""
	}
	s := &rv.HostStat{Cpu: 1, Memory: 2, Storage: 3, Network: 4, HostName: host}
	return s, err
}

func listImpl() (rv.TaskList, error) {
	taskMutex.Lock()
	defer taskMutex.Unlock()
	lst := rv.TaskList{}
	for _, v := range globalTaskList {
		lst.Tasks = append(lst.GetTasks(), v.task)
	}
	return lst, nil
}

func startImpl(req *rv.Task) (*rv.Task, error) {
	var (
		err      error
		stdout   io.ReadCloser
		stderr   io.ReadCloser
		procInfo processInfo
	)

	taskMutex.Lock()
	defer taskMutex.Unlock()

	log.Printf("Running: %s", req.GetAction())
	args := strings.Split(req.GetAction(), " ")
	var cmd *exec.Cmd
	if len(args) > 1 {
		cmd = exec.Command(args[0], args[1:]...)
	} else {
		cmd = exec.Command(req.GetAction())
	}

	stdout, err = cmd.StdoutPipe()
	if err != nil {
		log.Printf("Error: %v", err)
		return req, err
	}
	stderr, err = cmd.StderrPipe()
	if err != nil {
		log.Printf("Error: %v", err)
		return req, err
	}

	err = cmd.Start()
	if err != nil {
		log.Printf("Error: %v", err)
		return req, err
	}

	req.Id = uint32(cmd.Process.Pid)
	procInfo.task = req

	globalTaskList[req.GetId()] = procInfo

	go func() {
		var (
			n int
			e error
		)
		r := io.MultiReader(stdout, stderr)
		buff := make([]byte, 128)
		log.Println("Reading output...")
		for n, e = r.Read(buff); e != io.EOF; n, e = r.Read(buff) {
			log.Print(string(buff[:n]))
			for _, c := range procInfo.out {
				c <- string(buff[:n])
			}
		}
		log.Println("No more output")

		for _, c := range procInfo.out {
			close(c)
		}

		if e = cmd.Wait(); e != nil {
			log.Println(e)
		}
		log.Println("Deleting task entry")
		taskMutex.Lock()
		defer taskMutex.Unlock()
		delete(globalTaskList, req.GetId())
		log.Println("Deleted")
	}()

	return req, err
}

func outputStreamImpl(req *rv.Task, stream rv.Runner_OutputStreamServer) error {
	taskMutex.Lock()
	procInfo, ok := globalTaskList[req.GetId()]
	if !ok {
		taskMutex.Unlock()
		log.Fatalf("Process with PID %d not found", req.GetId())
		return errors.New("Task ID not found")
	}
	c := make(chan string, 16)
	procInfo.out = append(procInfo.out, c)
	taskMutex.Unlock()

	for data := range c {
		msg := &rv.Output{Stdout: data}
		if err := stream.Send(msg); err != nil {
			return err
		}
	}
	return nil
}

func kill(pid uint32, sig os.Signal) error {
	_, ok := globalTaskList[pid]
	if !ok {
		return errors.New("Task ID not found")
	}

	// Kill the process with sig
	proc, err := os.FindProcess(int(pid))
	if err != nil {
		log.Printf("Kill: error finding process %v", err)
		return err
	}
	err = proc.Signal(sig)
	if err != nil {
		log.Printf("Kill: error signaling process %v", err)
		return err
	}
	return err
}

func stopImpl(req *rv.Task) error {
	taskMutex.Lock()
	defer taskMutex.Unlock()

	//err := kill(req.GetId(), syscall.SIGTERM)
	err := kill(req.GetId(), syscall.SIGKILL)

	return err
}

func removeAllImpl() error {
	var err error
	taskMutex.Lock()
	defer taskMutex.Unlock()
	for pid := range globalTaskList {
		err = kill(pid, syscall.SIGKILL)
		if err != nil {
			return err
		}
	}
	globalTaskList = make(map[uint32]processInfo)
	return nil
}
