// +build !windows

package main

import (
	rv "gitlab.com/andreynech/raspvyp/proto"
	"syscall"
)

func suspendImpl(req *rv.Task) error {
	taskMutex.Lock()
	defer taskMutex.Unlock()

	err := kill(req.GetId(), syscall.SIGSTOP)
	return err
}

func resumeImpl(req *rv.Task) error {
	taskMutex.Lock()
	defer taskMutex.Unlock()

	err := kill(req.GetId(), syscall.SIGCONT)
	return err
}
