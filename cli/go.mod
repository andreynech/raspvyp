module gitlab.com/andreynech/raspvyp/cli

require (
	github.com/brutella/dnssd v0.0.0-20180519095852-a1eecd10aafc
	github.com/miekg/dns v1.0.8 // indirect
	gitlab.com/andreynech/raspvyp/proto v0.0.0-20180912102445-76c4a57c7ca6
	golang.org/x/crypto v0.0.0-20180910181607-0e37d006457b // indirect
	golang.org/x/net v0.0.0-20180911220305-26e67e76b6c3 // indirect
	google.golang.org/grpc v1.15.0
)
