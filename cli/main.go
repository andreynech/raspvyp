package main

import (
	"context"
	"flag"
	"github.com/brutella/dnssd"
	rv "gitlab.com/andreynech/raspvyp/proto"
	"google.golang.org/grpc"
	"log"
	"sort"
	"strconv"
	"time"
)

type RunnerInfo struct {
	svc   dnssd.Service
	proxy rv.RunnerClient
	stat  *rv.HostStat
}

const (
	srvType = "_raspvyp._tcp."
)

var (
	knownRunners []RunnerInfo
	domain       = flag.String("domain", "local.", "Set the search domain. For local networks, default is fine.")
	waitTime     = flag.Int("wait", 3, "Duration in [s] to run discovery.")
	timeFormat   = "15:04:05.000"
	list         = flag.Bool("l", false, "List all running tasks")
	start        = flag.Bool("s", false, "Start task. -c and -a arguments are required in this case")
	cGroup       = flag.String("g", "", "Control group")
	action       = flag.String("c", "", "The command to execute")
	stop         = flag.Bool("t", false, "Stop the task. -i and -h parameters are required in this case")
	pid          = flag.Int("p", -1, "Process id")
	targetHost   = flag.String("h", "", "Target host name")
	suspend      = flag.Bool("z", false, "Suspend task. -i and -h parameters are required in this case")
	resume       = flag.Bool("r", false, "Resume task. -i and -h parameters are required in this case")
	purge        = flag.Bool("x", false, "Remove all running tasks. If no -h provided, all hosts will be contacted")
)

func discover() {
	addFn := func(srv dnssd.Service) {
		log.Printf("Discovered\t%s\t%s\t%s\t%s\t%d\n", srv.Domain, srv.Type, srv.Name, srv.Host, srv.Port)
		if srv.Type == srvType {
			endpoint := srv.Hostname() + ":" + strconv.Itoa(srv.Port)
			// Set up a connection to the server.
			conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
			if err != nil {
				log.Fatalf("did not connect: %v", err)
			} else {
				log.Printf("Connected to %s", endpoint)
				scheduler := RunnerInfo{svc: srv, proxy: rv.NewRunnerClient(conn), stat: nil}
				knownRunners = append(knownRunners, scheduler)
			}
		}
	}

	rmFn := func(srv dnssd.Service) {
		endpoint := srv.Hostname() + ":" + strconv.Itoa(srv.Port)
		log.Printf("Disconnected from %s", endpoint)
		for i := range knownRunners {
			if knownRunners[i].svc.Equal(srv) {
				knownRunners = append(knownRunners[:i], knownRunners[i+1:]...)
				break
			}
		}
	}

	//ctx, cancel := context.WithCancel(context.Background())
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(*waitTime))
	defer cancel()

	service := srvType + *domain

	log.Printf("Discovering runners %s (%d sec)...", service, *waitTime)

	if err := dnssd.LookupType(ctx, service, addFn, rmFn); err != nil {
		log.Println(err)
		return
	}
}

func findRunner(host string) *RunnerInfo {
	for i := range knownRunners {
		if knownRunners[i].stat.GetHostName() == host {
			return &(knownRunners[i])
		}
	}

	return nil
}

func main() {

	flag.Parse()

	discover()
	log.Printf("%d hosts with runners found", len(knownRunners))
	if len(knownRunners) == 0 {
		log.Println("No runners found in subnet")
		return
	}

	empty := &rv.Empty{}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	for i := range knownRunners {
		runner := &(knownRunners[i])
		stat, err := runner.proxy.Stat(ctx, empty)
		if err == nil {
			//log.Println(stat)
			runner.stat = stat
		} else {
			log.Println(err)
		}
	}

	sort.Slice(knownRunners,
		func(i, j int) bool {
			return knownRunners[i].stat.Cpu < knownRunners[j].stat.Cpu
		})

	if *start {
		if cGroup == nil {
			log.Fatal("-c option required to schedule task")
		} else {
			if action == nil {
				log.Fatal("-a option required to schedule task")
			} else {
				// Task ID is ignored, hence id=0 here.
				// Returned task has properly initialized ID
				task := &rv.Task{ControlGroups: *cGroup, Action: *action}
				runner := knownRunners[0]
				entity, err := runner.proxy.Start(ctx, task)
				if err != nil {
					log.Fatalf("Error starting task: %v", err)
				} else {
					log.Printf("Task started with ID: %d on host %s",
						entity.GetId(), runner.stat.GetHostName())
				}
			}
		}
	}

	if *stop {
		if pid == nil {
			log.Fatal("-i option required to schedule task")
		} else {
			if len(*targetHost) == 0 {
				log.Fatal("-h option required to schedule task")
			} else {
				// Task ID is ignored, hence id=0 here.
				// Returned task has properly initialized ID
				runner := findRunner(*targetHost)
				if runner == nil {
					log.Fatalf("No registered runner on host %s", *targetHost)
				} else {
					task := &rv.Task{Id: uint32(*pid)}
					_, err := runner.proxy.Stop(ctx, task)
					if err != nil {
						log.Fatalf("Error stopping task: %v", err)
					} else {
						log.Printf("Task stopped")
					}
				}
			}
		}
	}

	if *suspend {
		if pid == nil {
			log.Fatal("-i option required to schedule task")
		} else {
			if len(*targetHost) == 0 {
				log.Fatal("-h option required to schedule task")
			} else {
				// Task ID is ignored, hence id=0 here.
				// Returned task has properly initialized ID
				runner := findRunner(*targetHost)
				if runner == nil {
					log.Fatalf("No registered runner on host %s", *targetHost)
				} else {
					task := &rv.Task{Id: uint32(*pid)}
					_, err := runner.proxy.Suspend(ctx, task)
					if err != nil {
						log.Fatalf("Error suspending task: %v", err)
					} else {
						log.Printf("Task suspended")
					}
				}
			}
		}
	}

	if *resume {
		if pid == nil {
			log.Fatal("-i option required to schedule task")
		} else {
			if len(*targetHost) == 0 {
				log.Fatal("-h option required to schedule task")
			} else {
				// Task ID is ignored, hence id=0 here.
				// Returned task has properly initialized ID
				runner := findRunner(*targetHost)
				if runner == nil {
					log.Fatalf("No registered runner on host %s", *targetHost)
				} else {
					task := &rv.Task{Id: uint32(*pid)}
					_, err := runner.proxy.Resume(ctx, task)
					if err != nil {
						log.Fatalf("Error resuming task: %v", err)
					} else {
						log.Printf("Task resumed")
					}
				}
			}
		}
	}

	if *list {
		for i := range knownRunners {
			runner := &(knownRunners[i])
			taskList, err := runner.proxy.List(ctx, empty)
			if err != nil {
				log.Fatal(err)
			} else {
				if len(taskList.GetTasks()) > 0 {
					log.Printf("On %s", runner.stat.GetHostName())
					for _, t := range taskList.GetTasks() {
						log.Printf("pid: %d cgroup: %s command: %s",
							t.GetId(), t.GetControlGroups(), t.GetAction())
					}
				}
			}
		}
	}

	if *purge {
		if len(*targetHost) > 0 {
			runner := findRunner(*targetHost)
			if runner == nil {
				log.Fatalf("No registered runner on host %s", *targetHost)
			} else {
				_, err := runner.proxy.RemoveAll(ctx, empty)
				if err != nil {
					log.Fatal(err)
				}
			}
		} else {
			for i := range knownRunners {
				runner := &(knownRunners[i])
				_, err := runner.proxy.RemoveAll(ctx, empty)
				if err != nil {
					log.Println(err)
				}
			}
		}
	}
}
